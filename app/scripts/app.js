'use strict';

/**
 * @ngdoc overview
 * @name ucxApiSampleApp
 * @description
 * # ucxApiSampleApp
 *
 * Main module of the application.
 */
angular
	.module('ucxApiSampleApp', [
		'ngAnimate',
		'ngCookies',
		'ngResource',
		'ngRoute',
		'angularFileUpload'
	])
	.config(function ($routeProvider, $locationProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'views/main.html',
				controller : 'MainCtrl'
			})
			.when('/documents/:page', {
				templateUrl: 'views/documents.html',
				controller : 'DocumentsCtrl'
			})
			.when('/documents', {
				templateUrl: 'views/documents.html',
				controller : 'DocumentsCtrl'
			})
			.when('/document/preview/:did', {
				templateUrl: 'views/document-preview.html',
				controller : 'DocumentPreviewCtrl'
			})
			.when('/document/detail/:did', {
				templateUrl: 'views/document-detail.html',
				controller : 'DocumentDetailCtrl'
			})
			.when('/document/upload', {
				templateUrl: 'views/document-upload.html',
				controller : 'DocumentUploadCtrl'
			})
			.when('/document/web-link/:did', {
				templateUrl: 'views/document-web-link.html',
				controller : 'DocumentWebLinkCtrl'
			})
			.when('/document/secure-send/:did', {
				templateUrl: 'views/document-secure-send.html',
				controller : 'DocumentSecureSendCtrl'
			})
			.when('/profile/edit', {
				templateUrl: 'views/profile-edit.html',
				controller : 'ProfileEditCtrl'
			})
			.when('/profile/change-password', {
				templateUrl: 'views/change-password.html',
				controller : 'ChangePasswordCtrl'
			})
			.otherwise({
				redirectTo: '/'
			});
//$locationProvider.html5Mode(true);
	})
	.constant('API_BASE_URL', 'https://dococloud.vagrant.foxitcloud.com/api');
