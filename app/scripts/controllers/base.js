'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:BaseCtrl
 * @description
 * # BaseCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('BaseCtrl', function ($scope, AuthService, Session, $location) {
		$scope.showSignModal = function(){
			$('#sign-in-modal').modal('show');
			return false;
		};
		$scope.showSignUpModal = function(){
			$('#sign-up-modal').modal('show');
			return false;
		};
		$scope.currentUser = null;
		$scope.isAuthorized = AuthService.isAuthenticated();
		if($scope.isAuthorized){
			$scope.currentUser = null;
			AuthService.loginByAccessToken(Session.access_token)
				.then(function (user) {
					$scope.setCurrentUser(user);
				});
		}

		$scope.setCurrentUser = function (user) {
			user.displayName = (user.lastname || user.firstname) ? user.firstname + " " + user.lastname : user.email;
			$scope.currentUser = user;
			$scope.isAuthorized = AuthService.isAuthenticated();
		};

		$scope.signOut = function(){
			Session.destroy();
			$scope.isAuthorized = AuthService.isAuthenticated();
		};

		$scope.isActive = function (url){
			return $location.$$url == url;
		}
  });
