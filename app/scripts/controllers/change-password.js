'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:DocumentDetailCtrl
 * @description
 * # DocumentDetailCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('ChangePasswordCtrl', function ($scope, AuthService, $location) {
		$scope.update = function(){
			AuthService.changePassword($scope.old_password, $scope.new_password)
				.then(function(){
					alert("Change password successful.");
					$location.path('/');
				},function(res){
					alert(res.data.message);
				});
		}
  });
