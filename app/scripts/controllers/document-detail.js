'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:DocumentDetailCtrl
 * @description
 * # DocumentDetailCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('DocumentDetailCtrl', function ($scope, $routeParams, document) {

		var did = $routeParams.did;
		$scope.document = null;
		document.getInfo(did)
			.then(function(data){
				$scope.document = data;
			});
		document.getShares(did)
			.then(function(shares){
				$scope.shares = shares;
			});
		$scope.update = function(){
			document.update($scope.document)
				.then(function(data){
					$scope.document = data;
				},function(msg){
					console.log(msg);
				});
		}
		$scope.deleteShare = function(index){
			document.deleteShare($scope.shares[index].share_id)
				.then(function(){
					alert("Delete share successful.");
					$scope.shares.splice(index,1);
				});
		}
  });
