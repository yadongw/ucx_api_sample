'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:DocumentPreviewCtrl
 * @description
 * # DocumentPreviewCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('DocumentPreviewCtrl', function ($scope, $routeParams, document, $location) {
		var did = $routeParams.did;
		document.getPreviewInfo(did)
			.then(function(res){
				$scope.images = res.data.images;
			},function(res){
				alert(res.data.message);
				window.location.reload();
			});
  });
