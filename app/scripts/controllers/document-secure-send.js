'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:DocumentWebLinkCtrl
 * @description
 * # DocumentWebLinkCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('DocumentSecureSendCtrl', function ($scope, $routeParams, document, $location) {
		var did = $routeParams.did;
		$scope.submit = function(){
			$scope.secureSendForm.$invalid = true;
			document.createSecureSend(did, $scope.share)
				.then(function(res){
					alert("Secure send successful");
					$location.path("/document/detail/" + did);
				},function(res){
					$scope.secureSendForm.email.$invalid = true;
					$scope.secureSendForm.email.$dirty = true;
					$scope.secureSendForm.email.error = res.data.message;
				});
		};
  });
