'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:DocumentUploadCtrl
 * @description
 * # DocumentUploadCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('DocumentUploadCtrl', function ($scope, FileUploader, API_BASE_URL, Session, $location) {
	var uploader = $scope.uploader = new FileUploader({
		alias: "pdf_file",
		url: API_BASE_URL + '/document?access-token=' + Session.access_token,
		//formData: {
		//	name: $scope.document.name,
		//	description: $scope.document.description
		//}
	});
	uploader.filters.push({
		name: 'customFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			return this.queue.length < 10;
		}
	});
    $scope.upload = function(){
	    if(uploader.queue.length == 0){
			return false;
	    }else{
		    uploader.queue[0].upload();
	    }
    };
	$scope.isChooseFile = function(){
		return uploader.queue.length > 0;
	}

	uploader.onSuccessItem = function(fileItem, response, status, headers) {
		alert("Upload file succesfully.")
		$location.path("/documents");
	};
  });
