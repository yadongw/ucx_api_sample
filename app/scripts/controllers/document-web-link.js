'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:DocumentWebLinkCtrl
 * @description
 * # DocumentWebLinkCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('DocumentWebLinkCtrl', function ($scope, $routeParams, document, $location) {
		var did = $routeParams.did;
		document.createWebLink(did)
			.then(function(share){
				$scope.share = share;
			});

		$scope.deactivate = function (sid){
			document.deleteShare(sid)
				.then(function(){
					alert("Delete Web Link Success");
					$location.path('/document/detail/' + did);
				});
		}
  });
