'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:DocumentsCtrl
 * @description
 * # DocumentsCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('DocumentsCtrl', function ($scope, $http, document, $routeParams, $location) {
		$scope.page = ($routeParams.page ? $routeParams.page : 1) * 1;
		document.getList($scope.page)
			.then(function(data){
				$scope.documents = data;
				$scope.count = $scope.documents.length;
			});

		$scope.isHasPrevious = function(){
			return $scope.page > 1;
		};
		$scope.isHasNext = function(){
			return $scope.count == 20;
		};

		$scope.download = function(did){
			window.open(document.getDownloadUrl(did));
			return false;
		}

		$scope.delete = function(documentInfo){
			document.delete(documentInfo.doc_id)
				.then(function(){
					$scope.documents.splice($scope.documents.indexOf(documentInfo), 1);
				})
			return false;
		}
  });
