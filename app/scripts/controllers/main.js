'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
