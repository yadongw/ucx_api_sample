'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:ProfileEditCtrl
 * @description
 * # ProfileEditCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
	.controller('ProfileEditCtrl', function ($scope, AuthService, Session) {
		AuthService.loginByAccessToken(Session.access_token)
			.then(function(user){
				$scope.user = user;
			});
		$scope.update = function () {
			AuthService.updateProfile($scope.user)
				.then(function(res){
					$scope.setCurrentUser(res.data);
					alert("Profile update successful.")
				},function(res){
					console.log(res);
				});
		}
	});
