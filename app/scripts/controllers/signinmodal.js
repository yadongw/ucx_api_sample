'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:SigninmodalCtrl
 * @description
 * # SigninmodalCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
	.controller('SigninmodalCtrl', function ($scope, $http, AuthService) {
		$scope.submit = function () {
			AuthService.login($scope.email, $scope.password)
				.then(function (response) {
					AuthService.saveAccessToken(response.data.access_token);
					AuthService.loginByAccessToken(response.data.access_token)
						.then(function (user){
							$scope.setCurrentUser(user);
						});
					$('#sign-in-modal').modal('hide');
				}, function (response) {
					$scope.userForm.password.$invalid = true;
					$scope.userForm.password.$dirty = true;
					$scope.userForm.password.error = response.data.message;
				});
		};
	});

