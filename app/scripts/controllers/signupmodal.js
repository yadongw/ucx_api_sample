'use strict';

/**
 * @ngdoc function
 * @name ucxApiSampleApp.controller:SignupmodalCtrl
 * @description
 * # SignupmodalCtrl
 * Controller of the ucxApiSampleApp
 */
angular.module('ucxApiSampleApp')
  .controller('SignupmodalCtrl', function ($scope, AuthService) {
		$scope.submit = function () {
			if($scope.password1 != $scope.password2){

				$scope.userForm.password2.$invalid = true;
				$scope.userForm.password2.$dirty = true;
				$scope.userForm.password2.error = "Two input password is not the same";
			}

			AuthService.signUp($scope.email, $scope.password1)
				.then(function(response){
					AuthService.saveAccessToken(response.data.access_token);
					AuthService.loginByAccessToken(response.data.access_token)
						.then(function (user){
							$scope.setCurrentUser(user);
						});
					$('#sign-up-modal').modal('hide');
					$scope.password1 = '';
					$scope.password2 = '';
					$scope.email = '';
				},function(response){
					$scope.userForm.password1.$invalid = true;
					$scope.userForm.password1.$dirty = true;
					$scope.userForm.password1.error = response.data.message;
				});
		}
  });
