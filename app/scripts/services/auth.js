'use strict';

/**
 * @ngdoc service
 * @name ucxApiSampleApp.auth
 * @description
 * # auth
 * Service in the ucxApiSampleApp.
 */
angular.module('ucxApiSampleApp')
	.factory('AuthService', function ($http, Session, API_BASE_URL) {
		var authService = {};

		authService.login = function (email, password) {
			return $http.post(API_BASE_URL + '/user/access-token', {email:email,password:password});
		};

		authService.signUp = function (email, password) {
			return $http.post(API_BASE_URL + '/user/sign-up', {email:email,password:password});
		};

		authService.loginByAccessToken = function (access_token) {
			return $http.get(API_BASE_URL + '/user/current?access-token=' + access_token)
				.then(function (res) {
					return res.data;
				});
		};

		authService.isAuthenticated = function () {
			return !!Session.access_token;
		};

		authService.saveAccessToken = function (access_token){
			Session.create(access_token);
		};

		authService.updateProfile = function(user){
			var access_token = Session.access_token;
			return $http.put(API_BASE_URL + '/user/current?access-token=' + access_token, user)
		};
		authService.changePassword = function(old_password, new_password){
			var access_token = Session.access_token;
			return $http.post(API_BASE_URL + '/user/change-password?access-token=' + access_token, {old_password:old_password, new_password:new_password});
		};
		return authService;
	});