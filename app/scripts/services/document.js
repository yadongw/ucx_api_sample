'use strict';

/**
 * @ngdoc service
 * @name ucxApiSampleApp.document
 * @description
 * # document
 * Service in the ucxApiSampleApp.
 */
angular.module('ucxApiSampleApp')
  .service('document', function ($http, API_BASE_URL, Session) {
        this.getList = function(page) {
	        return $http.get(API_BASE_URL + '/document?access-token=' + Session.access_token + '&page=' + page)
		        .then(function (res) {
			        return res.data;
		        },function(res){
			        return res.msg;
		        });
        };
		this.getPreviewInfo = function(did){
			return $http.get(API_BASE_URL + '/document/'+ did +'/preview?access-token=' + Session.access_token);
		};

		this.getInfo = function(did){
			return $http.get(API_BASE_URL + '/document/'+ did +'?access-token=' + Session.access_token)
				.then(function (res) {
					return res.data;
				}, function(res){
					return res.msg;
				});
		};

		this.update = function(document){
			var data = {'name':document.name, 'description': document.description};
			return $http.put(API_BASE_URL + '/document/'+ document.doc_id +'?access-token=' + Session.access_token, data)
				.then(function (res) {
					return res.data;
				}, function(res){
					return res.msg;
				});
		};

		this.getDownloadUrl = function(did){
			return API_BASE_URL + '/document/'+ did +'/download?access-token=' + Session.access_token
		};

		this.delete = function(did){
			return $http.delete(API_BASE_URL + '/document/'+ did +'?access-token=' + Session.access_token)
				.then(function (res) {
					return res.data;
				}, function(res) {
					return res.msg;
				});
		};

		this.createWebLink = function(did){
			return $http.post(API_BASE_URL + '/document/'+ did +'/share-public?access-token=' + Session.access_token)
				.then(function (res) {
					return res.data;
				}, function(res) {
					return res.msg;
				});
		};
		this.createSecureSend = function(did, share){
			return $http.post(API_BASE_URL + '/document/'+ did +'/share-private?access-token=' + Session.access_token, {email_list:[share.email],share_message:share.message});
		};
		this.deleteShare = function(sid){
			return $http.delete(API_BASE_URL + '/document/fake_id/share/'+ sid +'?access-token=' + Session.access_token)
				.then(function () {
					return true;
				}, function(res) {
					return res.msg;
				});
		};
		this.getShares = function(did){
			return $http.get(API_BASE_URL + '/document/'+ did +'/shares?access-token=' + Session.access_token)
				.then(function (res) {
					return res.data;
				}, function(res){
					return res.msg;
				});
		}
		return this;
  });
