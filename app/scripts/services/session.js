'use strict';

/**
 * @ngdoc service
 * @name ucxApiSampleApp.session
 * @description
 * # session
 * Service in the ucxApiSampleApp.
 */
angular.module('ucxApiSampleApp')
	.service('Session', function ($cookieStore) {
		this.access_token = $cookieStore.get("access_token");

//	AuthService.loginByAccessToken(this.access_token)
//		.then(function (user) {
//			$scope.setCurrentUser(user);
//		});

		this.create = function (access_token) {
			this.access_token = access_token;
			$cookieStore.put("access_token", access_token);
		};
		this.destroy = function () {
			this.access_token = null;
			$cookieStore.remove("access_token");
		};
		return this;
	});